AR-tower-configuration
=========

General tower configuration tasks. Tower integrations with JIRA, ElasticStack, HipChat, Bitbucket, etc.

Requirements
------------

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Author Information
------------------
